const {When, Then, After} = require ('@cucumber/cucumber');
const assert = require ('assert');
const {Builder, By, until} = require ('selenium-webdriver');
const chrome = require ('selenium-webdriver/chrome');
const chromedriver = require ('chromedriver');

When('we request the products list', async function (){
    chrome.setDefaultService(new chrome.ServiceBuilder(chromedriver.path).build());
    this.driver = new Builder()
        .forBrowser ('chrome')
        .build();

    this.driver.wait(until.elementLocated(By.className('products-list')));
    await this.driver.get('http://localhost:8000');
})