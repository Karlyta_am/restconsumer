import { LitElement, html, css } from 'lit-element';

class ProductsList  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        productos: {type: Array}
    };
  }

  constructor() {
    super();
    this.productos = [];
    this.productos.push({"nombre": "Móvil XL", "descripcion": "Telefóno grande con la mejor pantalla"});
    this.productos.push({"nombre": "Móvil Mini", "descripcion": "Telefóno mediano con la mejor cámara"});
    this.productos.push({"nombre": "Móvil Standard", "descripcion": "Telefóno estándar. Nada especial."});

  }

  render() {
    return html`
        <div class ="contenedor">
            ${this.productos.map(p => html ` <div class="producto"><h3>${p.nombre}</h3><p>${p.descripcion}</p></div>`)}
        </div>
    `;
  }
  
  createRenderRoot(){
    return this;
  }
}

customElements.define('products-list', ProductsList);