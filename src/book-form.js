import { LitElement, html, css } from 'lit-element';

class BookForm  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        id: {type: Number},
        titulo: {type: String},
        autor: {type: String}
    };
  }

  constructor() {
    super();
    this.id = 1;
    this.autor = "";
    this.titulo = "";
  }

  render() {
    return html`
        <div>
            <label for ="iid">ID</label>
            <input type="number" id="iid"  .value="${this.id}" @input = "${this.updateId}" />
            <br/>
            <label for ="ititulo">TITULO</label>
            <input type="text" id="ititulo"  .value="${this.titulo}" @input = "${this.updateTitulo}" />
            <br/>
            <label for ="iautor">AUTOR</label>
            <input type="text" id="iautor"  .value="${this.autor}" @input = "${this.updateAutor}"   />
            <br/>
            <button @click= "${this.buscarBook}">Buscar</button>
            <button @click= "${this.crearBook}">Crear</button>
            <button @click= "${this.modificarBook}">Modificar</button>
            <button @click= "${this.eliminarBook}">Eliminar</button>
        </div>
    `;
  }

  getCurrentBook(){
      var book = {};
      book.id = this.id;
      book.titulo = this.titulo;
      book.autor = this.autor;
      return book;
  }

  eliminarBook(){
    const options = {
        method: 'DELETE',
        body : "",
        headers: {'Content-Type': "application/json"}
    };  
    fetch("http://localhost:3381/books/" + this.id, options)
    .then(response => {
        console.log(response);
        if (!response.ok) { throw response;}
        return response.json();
    })
    .then(data => {
        alert("Libro eliminado");
    })
    .catch(error => {
        alert("Problemas con el fetch: " + error);
    })
  }

  modificarBook(){
    var book = this.getCurrentBook();
    const options = {
        method: 'PUT',
        body : JSON.stringify(book),
        headers: {'Content-Type': "application/json"}
    };
    fetch("http://localhost:3392/books", options )
    .then(response => {
        console.log(response);
        if (!response.ok) { throw response;}
        return response.json();
    })
    .then(data => {
      alert ("Book modificado");
    })
    .catch(error => {
        alert("Problemas con el fetch: " + error);
    })
}



  crearBook(){

      var book = this.getCurrentBook();
      const options = {
          method: 'POST',
          body : JSON.stringify(book),
          headers: {'Content-Type': "application/json"}
      };
      fetch("http://localhost:3392/books", options )
      .then(response => {
          console.log(response);
          if (!response.ok) { throw response;}
          return response.json();
      })
      .then(data => {
        alert ("Book creado");
      })
      .catch(error => {
          alert("Problemas con el fetch: " + error);
      })
  }



  buscarBook(){
    fetch("http://localhost:3392/books/" + this.id)
    .then(response => {
        console.log(response);
        if (!response.ok) { throw response;}
        return response.json();
    })
    .then(data => {
      this.id = data.id;
      this.titulo = data.titulo;
      this.autor = data.autor;
      console.log(data);
    })
    .catch(error => {
        alert("Problemas con el fetch: " + error);

    })
  }

  updateId(e){
    this.id = parseInt(e.target.value); 
  }
  updateTitulo(e){
    this.titulo = e.target.value; 
  }
  updateAutor(e){
    this.autor = e.target.value; 
  }
}

customElements.define('book-form', BookForm);